const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");
const https = require("https");

const app = express();
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/signup.html");
});
app.post("/", function(req, res) {
  const firstName = req.body.Fname;
  const lastName = req.body.Lname;
  const email = req.body.Ename;
  const data = {
    members: [
      {
        email_address: email,
        status: "subscribed",
        merge_fields: {
          FNAME: firstName,
          LNAME: lastName
        }
      }
    ]
  };
  const jsondata = JSON.stringify(data);
  console.log(firstName, lastName, email);
  const url = "https://us10.api.mailchimp.com/3.0/lists/bd3a5cec13"
  const option = {
    method: "POST",
    auth: "toneroblack:6386b07677edeb0de94d6a083899428f-us10"
  };
  const request = https.request(url, option, function(response) {
    if (response.statusCode == 200) {
      res.sendFile(__dirname + "/success.html");
    } else {
      res.sendFile(__dirname + "/failure.html");
    }
    response.on("data", function(data) {
      console.log(JSON.parse(data))


    });
  });
  request.write(jsondata);
  request.end();
});


app.listen(3000, function() {
  console.log("server is running on port 3000");
});
//API KEY
//6386b07677edeb0de94d6a083899428f-us10

//API ID
//bd3a5cec13
